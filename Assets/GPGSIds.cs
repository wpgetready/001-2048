// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class GPGSIds
{
        public const string achievement_the_pro__4096 = "CgkIz_ev3a4EEAIQBw"; // <GPGSID>
        public const string achievement_the_winner__2048 = "CgkIz_ev3a4EEAIQBg"; // <GPGSID>
        public const string leaderboard_2048_super_classic__highest_scores = "CgkIz_ev3a4EEAIQAQ"; // <GPGSID>
        public const string achievement_the_real_genius__16384 = "CgkIz_ev3a4EEAIQCQ"; // <GPGSID>
        public const string achievement_the_newby__256 = "CgkIz_ev3a4EEAIQAw"; // <GPGSID>
        public const string achievement_the_beginner__128 = "CgkIz_ev3a4EEAIQAg"; // <GPGSID>
        public const string achievement_the_impossible__32768 = "CgkIz_ev3a4EEAIQCg"; // <GPGSID>
        public const string achievement_the_master__8192 = "CgkIz_ev3a4EEAIQCA"; // <GPGSID>
        public const string achievement_being_serious__1024 = "CgkIz_ev3a4EEAIQBQ"; // <GPGSID>
        public const string achievement_getting_real__512 = "CgkIz_ev3a4EEAIQBA"; // <GPGSID>

}

