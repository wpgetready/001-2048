﻿
namespace SmartLocalization.Editor
{
    using UnityEngine.UI;
    using UnityEngine;
    using UnityEditor;
    using System.Collections;
    /// <summary>
    /// This is an expansion of SmartLocalization to work with this app
    /// sinc it is using LocalizedMeshText object which is diffent than Text object
    /// So I mirrored the LocalizedTextInspector / LocalizedText classes 
    /// creating analogies on LocalizedMeshTextInspector / LocalizedMeshText
    /// So far it worked quite alright, reducing code for translation in a 99%
    /// </summary>
    [CustomEditor(typeof(LocalizedMeshText))]
    public class LocalizedMeshTextInspector : Editor
    {
        private string selectedKey = null;

        void Awake()
        {
            LocalizedMeshText textObject = ((LocalizedMeshText)target);
            if (textObject != null)
            {
                selectedKey = textObject.localizedKey;
            }
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            selectedKey = LocalizedKeySelector.SelectKeyGUI(selectedKey, true, LocalizedObjectType.STRING);

            if (!Application.isPlaying && GUILayout.Button("Use Key", GUILayout.Width(70)))
            {
                LocalizedMeshText textObject = ((LocalizedMeshText)target);
                Undo.RecordObject(textObject, "Set Smart Localization text");
                textObject.localizedKey = selectedKey;
            }
        }

    }
}