﻿namespace SmartLocalization.Editor
{
    using UnityEngine;
    using UnityEngine.UI;
    using System.Collections;

    /// <summary>
    /// 20170531: FZSM
    /// Modified version of LocalizedText class for translation using tk2dTextMesh
    /// This is a customized adaptation to use SmartLocalization with object different than Text
    /// Is rather simple: just change class name and replace Text with tk2dTextMesh. Done!
    /// Notice that I also created a folder (uGUI) with similar name like the plugin is including, just to have a reference
    /// </summary>
    [RequireComponent(typeof(tk2dTextMesh))]
    public class LocalizedMeshText : MonoBehaviour
    {
        public string localizedKey = "INSERT_KEY_HERE";
        tk2dTextMesh textObject;

        void Start()
        {
            textObject = this.GetComponent<tk2dTextMesh>();

            //Subscribe to the change language event
            LanguageManager languageManager = LanguageManager.Instance;
            languageManager.OnChangeLanguage += OnChangeLanguage;

            //Run the method one first time
            OnChangeLanguage(languageManager);
        }

        void OnDestroy()
        {
            if (LanguageManager.HasInstance)
            {
                LanguageManager.Instance.OnChangeLanguage -= OnChangeLanguage;
            }
        }

        void OnChangeLanguage(LanguageManager languageManager)
        {
            textObject.text = LanguageManager.Instance.GetTextValue(localizedKey);
        }
    }
}