﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds;
using GoogleMobileAds.Api;


/// <summary>
/// Video Reward gives an option of seeing a video to add more undo's
/// </summary>
public class VideoRewardPopup : MonoBehaviour
{
    private RewardBasedVideoAd rewardBasedVideoAd;
    public tk2dUIItem closeBtn;
    public tk2dUIItem showVideoRewardBtn;
    //El uso del popupContainer obedece a lo siguiente:
    //Si deshabilito la clase VideoRewardPopup, inhibo la rutina de carga de los videos, lo que hace que los videos rewards solo se
    //carguen bajo demanda. Solución: VideoRewardPopup siempre está disponible, controlando solo la visibilidad del popup un container.
    //20170616: Debido a la continua cantidad de problemas dificiles de detectar con Video Reward, he decidido poner un log en TODAS partes 
    public GameObject popupContainer;

    private tk2dTextMesh txtButton;
    private string tmpText;
    //Pay attention following code is a VALID ONE for TESTING ONLY!
    public string AndroidAdUnitId; 
    /*
#if UNITY_EDITOR
    public string adUnitId = "unused";
#elif UNITY_ANDROID
   public            string adUnitId= "ca-app-pub-3940256099942544/5224354917";  //atención que este es un código DE PRUEBA, VALIDO para realizar tests!
#elif UNITY_IPHONE
         public   string adUnitId = "ca-app-pub-3940256099942544/1712485313";
#else
         public string adUnitId = "unexpected_platform";
#endif
*/

    private static bool eventCreated=false; //make sure events are created once
    private bool videoIsReady = false; //A video is ready when loaded event is fired
    private bool rewarded = false; //Flag to check if video was or not rewarded.

    public int undoQuantity;
    
    private GameManager _gameManager;
    public GameManager gameManager
    {
        get
        {
            if (_gameManager == null)
                _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            return _gameManager;
        }
    }

    void Start()
    {
        //Enable button functionality
        closeBtn.OnClick += hideBtn;
        showVideoRewardBtn.OnClick += showVideoReward;
        txtButton = showVideoRewardBtn.GetComponentInChildren<tk2dTextMesh>();//Obtener una referencia al texto del botón
        startSequence(); //Disable button if video isn't loaded yet
       }

    
    private void startSequence()
    {
        m("startSequence->waitForButton");
        waitForButton("LOADING...");
        m("startSequence->LoadRewardBasedAd");
        LoadRewardBasedAd();
    }

    private void waitForButton(string msg)
    {
        //Problem: if I disable the button , then a won't be able to enable it.
        //Reason: We are not in the main thread. We can manipulate enable if we are in Start/Awake, but outside of it we can't.
        //showVideoRewardBtn.enabled = false;
        tmpText = txtButton.text;
        txtButton.text = msg;
    }

    /// <summary>
    /// Source: https://developers.google.com/admob/unity/rewarded-video
    /// </summary>
    private void LoadRewardBasedAd()
    {
        string adUnitId = "unused";
        videoIsReady = false;
        rewardBasedVideoAd = null;
        rewardBasedVideoAd = RewardBasedVideoAd.Instance;
        if (!eventCreated)
        {
            m("LoadRewardBasedAd->Creando eventos...");
            rewardBasedVideoAd.OnAdRewarded += RewardBasedVideoAd_OnAdRewarded;
            rewardBasedVideoAd.OnAdFailedToLoad += RewardBasedVideoAd_OnAdFailedToLoad;
            rewardBasedVideoAd.OnAdLeavingApplication += RewardBasedVideoAd_OnAdLeavingApplication;
            rewardBasedVideoAd.OnAdLoaded += RewardBasedVideoAd_OnAdLoaded;
            rewardBasedVideoAd.OnAdClosed += RewardBasedVideoAd_OnAdClosed;
            eventCreated = true;
        }
        
#if UNITY_ANDROID
            adUnitId = AndroidAdUnitId.Trim();
            if (adUnitId.Length ==0 )
            {
                m("LoadRewardBasedAd->Problema ID EMPTY...");
            }
#elif UNITY_IPHONE
       adUnitId= "ca-app-pub-3940256099942544/5224354917";  //atención que este es un código DE PRUEBA, VALIDO para realizar tests!
#else
       adUnitId = "unexpected_platform";
#endif
        Debug.LogError("LoadRewardBasedAd->Loading ad...");
        rewardBasedVideoAd.LoadAd(new AdRequest.Builder().Build(), adUnitId); //an OnAdLoaded event will be fired when video is ready
    }

    /// <summary>
    /// Fire the event when reward video is loaded and ready to be displayed
    /// Enable also the button and restore text
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RewardBasedVideoAd_OnAdLoaded(object sender, System.EventArgs e)
    {
        videoIsReady = true;
        txtButton.text = tmpText; //restoring original text
                                  //what is unexplainable is WHY I can change the text but not the enable/disable ? Mistery here.
        m("onAdLoaded->VIDEO IS READY");
    }

    //Not set yet..
    private void RewardBasedVideoAd_OnAdLeavingApplication(object sender, System.EventArgs e)     { rewarded = false;    }
    //Not set yet..
    private void RewardBasedVideoAd_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)    { rewarded = false;   }

    /// <summary>
    /// Fired when user sees the video (without closing it)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RewardBasedVideoAd_OnAdRewarded(object sender, Reward e)
    {
        Debug.Log(string.Format("User Rewarded with {0} {1}!", e.Amount, e.Type));
        m("OnAdRewarded->VIDEO REWARDED");
        rewarded = true;
    }

    /// <summary>
    /// Due a bug, we need to re-set rewardBasedVideoAd to make sure ads can be displayed over again.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RewardBasedVideoAd_OnAdClosed(object sender, System.EventArgs e)
    {
        m("OnAdClosed->CLOSED VIDEO EVENT");
        startSequence();
        if (rewarded == true)
        {
            //VERY IMPORTANT: en el momento exacto que asigno esta variable, tengo una rutina en Game.cs que está chequeando el valor.
            //AUTOMATICAMENTE, se cerrará al asignarse el valor.
            m("OnAdClosedVIDEO CLOSED, counter assigned");
            UserData.countTemp = undoQuantity;
        } else
        {
            m("OnAdClosedVIDEO ABORTED, counter given -1");
            UserData.countTemp = -1; //Indico que el usuario no vio el video y no debe ser recompensado.
        }
        rewarded = false; //reiniciar el proceso.
        hide();
    }
    

    void hideBtn()
    {
        m("hideBtn->CLOSING WINDOW...");
        hide();
       // gameManager.hideVideoRewardpopup();
    }

    void showVideoReward()
    {
        m("showVideoReward->VIDEO READY TO DISPLAY?");
        if (videoIsReady)
        {
            m("showVideoReward->YES,DISPLAYING");
            rewardBasedVideoAd.Show();
        }
        
    }

    public void show()
    {
        m("show->SHOWING...");
        popupContainer.SetActive(true);
        //gameObject.SetActive(true);
    }

    public void hide()
    {
        m("hide->HIDING");
        popupContainer.SetActive(false);
        //gameObject.SetActive(false);
    }

    private void m (string msg)
    {
        Debug.Log(string.Format("------------------->{0}<-----------------------", msg));
    }
}

