﻿using UnityEngine;
using System.Collections;

public class SocialButtons : MonoBehaviour
{
    public tk2dUIItem btnLeaderBoard;
    public tk2dUIItem btnAchievement;

    private GameManager _gameManager;
    public GameManager gameManager

    {
        get
        {
            if (_gameManager == null)
                _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

            return _gameManager;
        }
    }

    void Start()
    {
        btnLeaderBoard.OnClick += clickLeaderBoard;
        btnAchievement.OnClick += clickAchievements;
        
    }

    void Update()
    {

    }

    void clickLeaderBoard()
    {
        GameManager.gamecenter.showLeaderboard();
    }

    void clickAchievements()
    {
        GameManager.gamecenter.showAchievementUI();
    }


}

