﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    #if UNITY_IOS && !UNITY_EDITOR
    static public GamecenterProxy gamecenter = new IOSGamecenterProxy();
    #elif UNITY_ANDROID && !UNITY_EDITOR
    static public GamecenterProxy gamecenter = new AndroidGamecenterProxy();
    #else
    static public GamecenterProxy gamecenter = new GamecenterProxy();
    #endif

    static private bool _GAME_RUNNING;
    static public bool GAME_RUNNING
    {
        get
        {
            return _GAME_RUNNING;
        }
        set
        {
            _GAME_RUNNING = value;
            SwipeManager.swipeDirection = SwipeManager.Swipe.None;
        }
    }

    private PluginsProxy _plugins;
    public PluginsProxy plugins
    {
        get
        {
            if (_plugins == null)
            {
                _plugins = GameObject.Find("PluginsController").GetComponent<PluginsProxy>();
            }

            return _plugins;
        }
    }

    public Game game;
    public StartScreen startScreen;
    public GameOverScreen gameOverScreen;
    public OptionsScreen optionsScreen;
    public Tutorial tutorial;
    public WinPopup winPopup;
    public UndoPopup undoPopup;
    public VideoRewardPopup videoRewardPopup;

    public int score;

    private IScreen _curScreen;

    void Start()
    {
        gamecenter.init();
        GAME_RUNNING = false;
        game.hide();
        game.init();
        gameOverScreen.hide();
        optionsScreen.hide();
        tutorial.hide();
        winPopup.hide();
        undoPopup.hide();
        videoRewardPopup.hide();

        _curScreen = startScreen;
        _curScreen.show();
    }

    public void startGame()
    {
        _showScreen(game);
        game.restart();
    }

    public void showTutorial()
    {
        GAME_RUNNING = false;
        _showScreen(tutorial);
    }

    public void showWinpopup(int score)
    {
        plugins.showBanner();
        this.score = score;
        GAME_RUNNING = false;
        winPopup.show();
    }

    public void hideWinpopup(bool needRestart)
    {
        GAME_RUNNING = true;
        winPopup.hide();
        if (needRestart)
            game.restart();
    }

    public void showUndopopup()
    {
        GAME_RUNNING = false;
        undoPopup.show();
    }

    public void hideUndopopup()
    {
        GAME_RUNNING = true;
        undoPopup.hide();
    }

    public void showVideoRewardpopup()
    {
        GAME_RUNNING = false;
        videoRewardPopup.show();
    }
    
    
    public void hideVideoRewardpopup()
    {
        Debug.Log("--->HIDING REWARD POPUP, GAMEMANAGER");
        videoRewardPopup.hide();
        GAME_RUNNING = true;
    }

    public void continueGame()
    {
        _showScreen(game);
        GAME_RUNNING = true;
    }

    public void showOptions()
    {
        GAME_RUNNING = false;
        _showScreen(optionsScreen);
    }

    public void showGameOver(int score)
    {
        plugins.showBanner();
        this.score = score;
        GAME_RUNNING = false;
        gameOverScreen.init(score);
        _showScreen(gameOverScreen);
    }
        
    private void _showScreen(IScreen screen)
    {
        _curScreen.hide();
        _curScreen = screen;
        _curScreen.show();
    }
}
