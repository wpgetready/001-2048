using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SocialPlatforms.GameCenter;

public class IOSGamecenterProxy:GamecenterProxy
{
    protected override void _initIDs()
    {
        _leaderboardId = Settings.instance.leaderboardIOS;
        GameCenterPlatform.ShowDefaultAchievementCompletionBanner(false);
    }
        
    public override void showLeaderboard()
    {
        if (_logged)
        {
            //Does not show What I tried tod display, so I work in something simpler
            //GameCenterPlatform.ShowLeaderboardUI(_leaderboardId, UnityEngine.SocialPlatforms.TimeScope.Week);
            Social.ShowLeaderboardUI();

        }
        else
        {
            _needShowLeaderboards = true;
            _authenticate(); 
        }
    }
}

