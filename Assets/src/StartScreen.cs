﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class StartScreen : MonoBehaviour, IScreen
{
  
    public tk2dUIItem playBtn;
    /*
  public tk2dTextMesh txtPlayBtn;
  public tk2dTextMesh txtMoreGames;
  public tk2dTextMesh txtRestorePurchase; //this is a graphic.
  public tk2dTextMesh txtRemoveAds;
  private LanguageManager lm;
  */
  
    private GameManager _gameManager;
    public GameManager gameManager
    {
        get
        {
            if (_gameManager == null)
                _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

            return _gameManager;
        }
    }

    void Start()
    {

        LanguageManager.Instance.ChangeLanguage("es");
        playBtn.OnClick += startGame;
       
//        translate();
    }

    /*
    private void translate()
    {
        string systemLanguage = Application.systemLanguage.ToString();
        
        lm = LanguageManager.Instance;
        lm.ChangeLanguage("es");
        txtPlayBtn.text = lm.GetTextValue("StartScreen.Play");
        txtMoreGames.text = lm.GetTextValue("StartScreen.MoreGames");
        txtRestorePurchase.text = lm.GetTextValue("StartScreen.RestorePurchase");
        txtRemoveAds.text = lm.GetTextValue("StartScreen.RemoveAds");
    }
    */

    void startGame()
    {
     

        if (UserData.isFirstPlay)
            gameManager.showTutorial();
        else
            gameManager.startGame();
    }

    void Update()
    {
	
    }

    public void show()
    {
        gameObject.SetActive(true);
    }

    public void hide()
    {
        gameObject.SetActive(false);
    }

    public void init()
    {
    }
}
