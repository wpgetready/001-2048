﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using System.Threading;

public class SceneSplash : MonoBehaviour {

    //20160702: Notas
    //Este script afecta la transparencia de un material determinado que se pasa por parámetro
    //Es importante tener en cuenta:
    //1-El material tiene que tener rendering mode FADE, de lo contrario  no se aprecia el efecto.
    //2-Este script puede conectarse a cualquier objeto, puesto que no usa ningun parámetro del mismo, este script
    //solo modifica un material. 
    //3-Tiene tres tiempos de transicion: Entrada/Despliegue/Salida
    // 3' Le agregue un tiemp de transición entre la salida y el salto a la siguiente escena, para que no sea muy brusco.
    //4-Tiene dos valores de transparencia: Inicial (generalmente 0 - transparente) y no transparente (1)

    public string NextScene = "World"; //Siguiente escena a levantar despues del splash
 //   public Text displayInfo; 


    public Material SplashMaterial = null;

    public int TransitionIn = 1500;
    public int TransitionStay = 2000;
    public int TransitionOut = 1500;
    public int TransitionBeforeNextScene = 1000;

    public float MinAlpha = 0.1f;
    public float MaxAlpha = 1.0f;

    private DateTime m_timerIn = DateTime.MinValue;
    private DateTime m_timerStay = DateTime.MinValue;
    private DateTime m_timerOut = DateTime.MinValue;
    private DateTime m_timerTBNS = DateTime.MinValue;

    private float AlphaToApply;

    private bool loggedWasProcessed=false;

    // Use this for initialization
    void Start()
    {
        m_timerIn = DateTime.Now + TimeSpan.FromMilliseconds(TransitionIn);
        m_timerStay = m_timerIn + TimeSpan.FromMilliseconds(TransitionStay);
        m_timerOut = m_timerStay + TimeSpan.FromMilliseconds(TransitionOut);
        m_timerTBNS = m_timerOut + TimeSpan.FromMilliseconds(TransitionBeforeNextScene);

        AlphaToApply = MinAlpha;

     //   Social.localUser.Authenticate(onLogged); //Intento loguearme, pasando un callback donde se  devuelve el resultado del pedido
    }

    /*
    private void onLogged (bool success)
    {
        loggedWasProcessed = true; //This flag means the google play logging returned
        if (success)
        {
            Debug.Log( "Google Play authentification success \n");
            displayInfo.text = "Login Success!";
        }
        else
        {
            Debug.Log ("Google Play authentification failed \n");
            displayInfo.text = "Login Failed....";
        }
    }
    */
    void OnEnable()
    {
        SplashMaterial.SetColor("_Color", new Color(1, 1, 1, MinAlpha));
    }
    void OneDestroy()
    {
        SplashMaterial.SetColor("_Color", new Color(1, 1, 1, MinAlpha));
    }
    // Update is called once per frame
    void Update()
    {

        if (m_timerTBNS < DateTime.Now)
        {
            SceneManager.LoadScene(NextScene);
        }

        if (m_timerStay < DateTime.Now)
        {
            int elapsed = (int)(m_timerOut - DateTime.Now).TotalMilliseconds;
            AlphaToApply = Mathf.Max(MinAlpha, Mathf.InverseLerp(MinAlpha, TransitionOut, elapsed) * MaxAlpha);
    //        Debug.Log("Transparencia: " + AlphaToApply);
        }
        else if (m_timerIn < DateTime.Now)
        {
            AlphaToApply = MaxAlpha;
        }
        else
        {
            int elapsed = (int)(m_timerIn - DateTime.Now).TotalMilliseconds;
            AlphaToApply = Mathf.Max(MinAlpha, Mathf.InverseLerp(TransitionOut, MinAlpha, elapsed) * MaxAlpha);
        }
        SplashMaterial.SetColor("_Color", new Color(1, 1, 1, AlphaToApply));

    }

    /// <summary>
    /// Este es un método que hace varias cosas:
    /// Por un lado espera si nos pudimos loguear correctamente.
    /// Si tuvimos éxito, perfecto, en caso contrario tambien, pero no podremos usar las características de Google Play
    /// HAY QUE TENER EN CUENTA AQUELLOS USUARIOS QUE NO PUEDAN ACCEDER AL SERVICIO.
    /// </summary>
    void startGame()
    {
        int counter = 0;
        while ((!loggedWasProcessed) && (counter<=5))
        {
            counter++;
           // displayInfo.text = string.Format("Wait ({0})", counter);
            Thread.Sleep(1000);
        }
        SceneManager.LoadScene(NextScene);
        return;
    }

  }
