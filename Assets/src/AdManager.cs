﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdManager : MonoBehaviour {


    private RewardBasedVideoAd rewardBasedVideoAd;
    private static bool isReady; // video is ready to be displayed
    private static bool isRewarded; //video was played and rewarded
    private static bool isAborted; //video was aborted or some error was raised.

    public static AdManager instance;
    public string AndroidID;
    public string IphoneID;

    /// <summary>
    /// Singleton creation
    /// </summary>
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    /// <summary>
    ///Android Test     private RewardBasedVideoAd rewardBasedVideoAd;
    ///Iphone Test string adUnitId = "ca-app-pub-3940256099942544/1712485313";
    /// </summary>
    private void LoadRewardBasedAd()
    {
        rewardBasedVideoAd = RewardBasedVideoAd.Instance;
        rewardBasedVideoAd.OnAdRewarded += RewardBasedVideoAd_OnAdRewarded;
        rewardBasedVideoAd.OnAdClosed += RewardBasedVideoAd_OnAdClosed;
        rewardBasedVideoAd.OnAdFailedToLoad += RewardBasedVideoAd_OnAdFailedToLoad;
        rewardBasedVideoAd.OnAdLeavingApplication += RewardBasedVideoAd_OnAdLeavingApplication;
        rewardBasedVideoAd.OnAdLoaded += RewardBasedVideoAd_OnAdLoaded;
        rewardBasedVideoAd.OnAdOpening += RewardBasedVideoAd_OnAdOpening;
        rewardBasedVideoAd.OnAdStarted += RewardBasedVideoAd_OnAdStarted;

        isReady = false;
        isRewarded = false;
        isAborted = false;


        #if UNITY_EDITOR
        string adUnitId = "unused";
        #elif UNITY_ANDROID
                string adUnitId = AndroidID;
        #elif UNITY_IPHONE
                            string adUnitId= IphoneID;
        #else
                            string adUnitId = "unexpected_platform";
        #endif
        try
        {
            rewardBasedVideoAd.LoadAd(new AdRequest.Builder().Build(), adUnitId);
            //The video will be available only when isLoaded=true;
        }
        catch (System.Exception e)
        {
            Debug.Log("LRBA Error: " + e.Message);
        }
    }

    /// <summary>
    /// Due some issues under Android, we need to destroy and reconstructo the rewardBasedVideoAd object)
    /// </summary>
    private void destroyRewardedBaseAd()
    {
        if (rewardBasedVideoAd != null)
        {
            rewardBasedVideoAd.OnAdRewarded -= RewardBasedVideoAd_OnAdRewarded;
            rewardBasedVideoAd.OnAdClosed -= RewardBasedVideoAd_OnAdClosed;
            rewardBasedVideoAd.OnAdFailedToLoad -= RewardBasedVideoAd_OnAdFailedToLoad;
            rewardBasedVideoAd.OnAdLeavingApplication -= RewardBasedVideoAd_OnAdLeavingApplication;
            rewardBasedVideoAd.OnAdLoaded -= RewardBasedVideoAd_OnAdLoaded;
            rewardBasedVideoAd.OnAdOpening -= RewardBasedVideoAd_OnAdOpening;
            rewardBasedVideoAd.OnAdStarted -= RewardBasedVideoAd_OnAdStarted;
            rewardBasedVideoAd = null;
        }
        isReady = false;
        isRewarded = false;
        isAborted = false;
    }

    /// <summary>
    /// Waits for video is ready and launch it.
    /// </summary>
    /// <returns></returns>

    public IEnumerator waitIsLoaded ()
    {
        while (!rewardBasedVideoAd.IsLoaded())
        {
            yield return null;
        }
        isReady = true;
        rewardBasedVideoAd.Show();
    }


    private void RewardBasedVideoAd_OnAdStarted(object sender, System.EventArgs e)
    {
       // throw new System.NotImplementedException();
    }

    private void RewardBasedVideoAd_OnAdOpening(object sender, System.EventArgs e)
    {
        //throw new System.NotImplementedException();
    }

    private void RewardBasedVideoAd_OnAdLoaded(object sender, System.EventArgs e)
    {
        //throw new System.NotImplementedException();
        //This fire when it gets ready?
        //Puedo generar un delegado?
    }

    private void RewardBasedVideoAd_OnAdLeavingApplication(object sender, System.EventArgs e)
    {
        throw new System.NotImplementedException();
    }

    private void RewardBasedVideoAd_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        throw new System.NotImplementedException();
    }

    private void RewardBasedVideoAd_OnAdClosed(object sender, System.EventArgs e)
    {
        throw new System.NotImplementedException();
    }

    private void RewardBasedVideoAd_OnAdRewarded(object sender, Reward e)
    {
        throw new System.NotImplementedException();
    }
}
